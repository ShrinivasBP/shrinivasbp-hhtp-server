const path= require("path")
const uuid = require("uuid")
const fs = require("fs")
const UUID_FILE_PATH=path.join(__dirname,"../../UUID.json")
function UUID(){
let generatedUUID = uuid.v4()
let display= {
    "uuid" : generatedUUID
}
fs.writeFile(UUID_FILE_PATH, JSON.stringify(display), "utf-8", (error)=>{
    if(error){
        console.log(error)
    }
})
}


module.exports={ UUID : UUID}