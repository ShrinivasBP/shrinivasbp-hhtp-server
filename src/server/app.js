const path= require("path")
const http= require("http")
const fs = require("fs")
const uuid=require(path.join(__dirname,"../public/UUID.js"))
const PORT_NUM=8080

function readData(filePath){
    return new Promise((resolve, reject)=>{
        fs.readFile(filePath,"utf-8",(error,fileData)=>{
            if(error){
                reject(error)
            }
            else{
                resolve(fileData)
            }

        })
    })
}


let server= http.createServer((request, response)=>{
    url = request.url
    console.log(url)
    switch(true){

        case ((/^\/$/).test(url)):{
            readData(path.join(__dirname,"../public/index.html"))
            .then((html)=>{
                response.writeHead(200,{ "Content-Type" : "text/html"})
                response.write(html)
                response.end()
            })
            .catch(()=>{
                response.writeHead(404)
                response.write("File not found...")
                response.end()
            })
            
            break;
        }

        case ((/^\/html$/).test(url)):{
            readData(path.join(__dirname,"../public/message.html"))
            .then((html)=>{
                response.writeHead(200,{ "Content-Type" : "text/html"})
                response.write(html)
                response.end()
            })
            .catch(()=>{
                response.writeHead(404)
                response.write("File not found...")
                response.end()
            })
            
            break;
        }
        
        case ((/^\/json$/).test(url)):{
            readData(path.join(__dirname,"../public/string.json"))
            .then((json)=>{
                response.writeHead(200,{ "Content-Type" : "application/json"})
                response.write(json)
                response.end()
            })
            .catch(()=>{
                response.writeHead(404)
                response.write("File not found...")
                response.end()
            })     
            break;
        }        

        case ((/^\/uuid.js$/).test(url)):{
            readData(path.join(__dirname,"../public/UUID.js"))
            .then((js)=>{

               
                response.writeHead(200, {"Content-Type" : "text/js"})
                response.write(js)
                response.end()
            })
            .catch(()=>{
                response.writeHead(404)
                response.write("File not found...")
                response.end()
            })
            break;t
        }

        case ((/^\/uuid$/).test(url)):{
            readData(path.join(__dirname,"../../UUID.json"))
            .then((json)=>{
                uuid.UUID()
                
                response.writeHead(200, {"Content-Type" : "application/json"})
                response.write(json)
                response.end()
            })
            .catch(()=>{
                response.writeHead(404)
                response.write("File not found...")
                response.end()
            })
            break;
        }
                
                
        case ((/^\/json\/status\/(100|200|300|400|500)$/).test(url)):{
            
            readData(path.join(__dirname, "../public/status.html"))
            .then((html)=>{      
                        
                response.writeHead((url.match(/[0-9]{3}$/)).toString(),{ "Content-Type": "text/html"})
                if((url.match(/[0-9]{3}$/)).toString()==100){
                    response.write("Every thing is fine you can continue with the response")
                }
                else{
                response.write(html+(url.match(/[0-9]{3}$/)).toString())
                }
                response.end()
            })
            .catch(()=>{
                response.writeHead(404)
                response.write("File not found...")
                response.end()
            })   
            break;         

        }
        case ((/^\/json\/delay\/[0-9]$/).test(url)):{          
            try{
                setTimeout(()=>{
                    response.writeHead(200,{ "Content-Type": "text/html"})
                    response.write("hello delay")
                    response.end()
                },1000*(url.match(/[0-9]/)).toString())    
            }
            catch(error){
                response.writeHead(404)
                response.end()
            }     
            
            break;    
        }
            

        default : {
            response.writeHeader(404) , {'content-type': 'text/html'};
            response.write("Page not found");
            response.end();
        }
    }
})

.listen(PORT_NUM,(error)=>{
    if(error){
        console.error(error)
    }
    else{
        console.log("Server is listening at port : "+PORT_NUM)
    }
})